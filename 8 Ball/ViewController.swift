//
//  ViewController.swift
//  8 Ball
//
//  Created by Newarpunk on 8/16/19.
//  Copyright © 2019 Akash Stha. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    var imageArray = ["ball1","ball2","ball3","ball4","ball5"]
    
    var randomBall : Int = 0

    @IBOutlet weak var askMeBall: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        askMe()
    }
    
    override func motionEnded(_ motion: UIEvent.EventSubtype, with event: UIEvent?) {
        askMe()
    }

    @IBAction func askMeButton(_ sender: Any) {
        askMe()
    }
    
    func askMe(){
    
        randomBall = Int.random(in: 0 ... 4)
    
        askMeBall.image = UIImage(named: imageArray[randomBall])
    
    }
    
}

